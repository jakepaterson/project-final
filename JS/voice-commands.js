
function scroll_down(){
    //Scrolls the window 2 pixels vertically
    //the zero represents the horizontal scrooll value
    window.scrollBy(0,25);
    setTimeout(scroll_down,5);
}
function scroll_up(){
    window.scrollBy(0,-25);
     //scroll_up function will take ten seconds to compleate
     setTimeout(scroll_down,5);
}



window.onload = function(){
  if(annyang){
    var commands = {
      'Scroll Down': function(){
        scroll_down();

      },
      'Stop': function(){
        clearTimeout(scroll_down);
        clearTimeout(scroll_up);
      },

      'Scroll up': function(){
        scroll_up();
      },

      'go to top':function(){
        window.scrollTo(0,0);
      },

      'go to bottom': function(){
        window.scrollTo(0,10000);
      },

      'go back':function(){
        window.history.go(-1);
      },

      'go forward':function(){
        window.history.go(+1);
      },

      'go to home':function(){
        window.location.assign('index.html');
      },

      'go to film':function(){
        window.location.assign('film.html');
      },

      'go to gaming':function(){
        window.location.assign('gaming.html');
      },

      'go to music':function(){
        window.location.assign('music.html');
      },

      'go to weird stuff':function(){
        window.location.assign('weird.html');
      },
      'go to kendrick and character':function(){
        window.location.assign('article1.html')
      },
      'Go to the why of Lo-fi':function(){
        window.location.assign('lofi.html')
      },
      'Go to Surviving Shibata':function(){
        window.location.assign('shibata.html')
      },
      'Go to bullets and feminism':function(){
        window.location.assign('mad-max.html')
      },
    }
  }
    annyang.addCommands(commands);
    annyang.start();
};


//function scroll_down(){
    //Scrolls the window 2 pixels vertically
    //the zero represents the horizontal scrooll value
  //  window.scrollBy(0,2);

    //scroll_down function will take ten seconds to compleate
    // var scrolling = setTimeout(scroll_down,10);
//}
//function scroll_up(){
  //  window.scrollBy(0,-2);
     //scroll_up function will take ten seconds to compleate
    // var scrolling = setTimeout(scroll_down,10);
//}
